# Some random sounds

define :ran1 do
  rand
end

define :rdef do |seed|
  with_random_seed seed do
    [
      [0, ran1, ran1, ran1, ran1],
      [1, ran1, ran1, ran1, ran1],
      [2, ran1, ran1, ran1, ran1],
      [3, ran1, ran1, ran1, ran1],
      [4, ran1, ran1, ran1, ran1],
      [5, ran1, ran1, ran1, ran1],
      [6, ran1, ran1, ran1, ran1],
      [7, ran1, ran1, ran1, ran1],
      [8, ran1, ran1, ran1, ran1],
      [9, ran1, ran1, ran1, ran1],
      [10, ran1, ran1, ran1, ran1],
      [11, ran1, ran1, ran1, ran1],
      [12, ran1, ran1, ran1, ran1],
      [13, ran1, ran1, ran1, ran1],
      [14, ran1, ran1, ran1, ran1],
      [15, ran1, ran1, ran1, ran1],
      [16, ran1, ran1, ran1, ran1],
      [17, ran1, ran1, ran1, ran1],
      [18, ran1, ran1, ran1, ran1],
      [19, ran1, ran1, ran1, ran1],
      [20, ran1, ran1, ran1, ran1],
      [21, ran1, ran1, ran1, ran1],
      [22, ran1, ran1, ran1, ran1],
      [23, ran1, ran1, ran1, ran1],
      [24, ran1, ran1, ran1, ran1],
      [25, ran1, ran1, ran1, ran1],
      [26, ran1, ran1, ran1, ran1],
      [27, ran1, ran1, ran1, ran1],
      [28, ran1, ran1, ran1, ran1],
      [29, ran1, ran1, ran1, ran1],
    ]
  end
end

define :s0 do |pitch, duration, sdef|
  use_synth :sine
  n = 1
  inter = 3
  for sy in sdef
    in_thread do
      play pitch: pitch + (sy[0] * (12 / 3)),
        duration: duration * sy[1],
        attack: duration * sy[2] * 0.5,
        sustain: duration * sy[3] * 0.2,
        amp: sy[4] *0.6
    end
    n = n+1
  end
end

define :sr do |pitch, duration, seed|
  d = rdef seed
  s0 pitch, duration, d
end

define :fdurs do
  [0.5, 0.5, 1, 0.25, 0.25, 0.5, 1].ring
end
define :fpis do
  [10, 12, 15, 15, 15, 12, 10, 10, 10].ring
end

live_loop :t1 do
  k = 0
  100.times do
    pis = fpis
    durs = fdurs
    de = rdef k
    7.times do
      s0 pis[k], durs[k], de
      sleep durs[k]
      k = k + 1
    end
  end
end
