# first tryout


define :s1 do |pitch, duration|
  use_synth :sine
  s1_def = [
    [0, 1.0, 0.2],
    [1, 0.1, 0.05],
    [2, 0.5, 0.1],
    [3, 1.0, 0.05],
    [4, 0.3, 0.1],
    [5, 0.3, 0.2],
    [6, 0.3, 0.2],
  ]
  for s1 in s1_def
    in_thread do
      play pitch: pitch + (s1[0] * 12),
        duration: duration * s1[1],
        amp: s1[2]
    end
  end
end

live_loop :t1 do
  pis = [20, 22, 24, 13, 12]
  durs = [0.1, 0.2, 0.2, 1, 1]
  pis.length.times do |i|
    s1 pis[i], durs[i]
    sleep durs[i]
  end
end