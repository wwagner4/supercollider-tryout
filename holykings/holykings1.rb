live_loop :holykings1 do
  use_synth :saw
  play 56, amp: 0.1, attack: 0.8, sustain: 0.2, release: 2
  sleep 0.5
  play 55, amp: 0.3, attack: 0.1, sustain: 0.2, release: 1
  sleep 0.5
  play 54, amp: 0.1, attack: 0.005, sustain: 0.2, release: 1
  sleep 0.5
  use_synth :prophet
  play 66, amp: 0.4, pan: -0.7
  sleep 0.25
  play 66, amp: 0.3, pan: -0.9
  sleep 0.25
  play 66, amp: 0.2, pan: 0.1
  sleep 0.25
  play 66, amp: 0.1, pan: 0.3
  sleep 0.25
  play 66, amp: 0.05, pan: 0.6
  sleep 0.25
  play 66, amp: 0.05, pan: 0.7
  sleep 0.25
end
