
pan = 0.7
pandiff = -0.03
cnt = 0

live_loop :holykings6 do
  print(cnt)
  in_thread do
    piss1 = [
      [55, 66, 0, 0, 60, 65, 0].ring,
      [55, 0, 0, 0, 0, 0, 0].ring,
      [0, 0, 0, 0, 0, 0].ring,
      [0, 0, 0, 0, 0, 0, 0].ring
    ]
    piss2 = [
      [65, 65, 61, 58, 58, 0, 60].ring,
      [60, 60, 65, 65, 58, 55, 55].ring,
      [60, 60, 65, 65, 58, 55, 55].ring,
      [70, 70, 75, 75, 68, 65, 65].ring,
      [55, 65, 65, 0, 58, 0, 60].ring,
      [60, 65, 0, 0, 60, 65, 55].ring,
      [55, 0, 0, 0, 60, 65, 0].ring,
      [55, 55, 0, 0, 0, 0, 0].ring,
      [65, 0, 0, 0, 0, 0, 0].ring,
      [66, 66, 0, 0, 0, 0, 0].ring
    ]
    piss3 = [
      [65, 65, 61, 58, 58, 0, 60].ring,
      [60, 60, 65, 65, 58, 55, 55].ring,
      [60, 60, 65, 65, 58, 55, 55].ring,
      [70, 70, 75, 75, 68, 65, 65].ring,
      [75, 65, 75, 70, 58, 0, 60].ring
    ]
    use_synth :fm
    rel = 0.1
    amp = 1
    pi = 0
    if cnt < 10
      pis = piss1.choose
      print("a from piss1 " + pis.to_s)
    elsif cnt < 20
      pis = piss2.choose
      print("a from piss2 " + pis.to_s)
    else
      pis = piss3.choose
      print("a from piss3 " + pis.to_s)
    end
    define :melody1 do
      play pis[pi], amp: amp, attack: 0.01, sustain: 0, release: rel
      rel *= 1.4
      amp *= 0.85
      pi += 1
      sleep 0.25
    end
    
    12.times do
      melody1
    end
    sleep 3
  end
  in_thread do
    piss1 = [
      [65, 66].ring,
      [0, 66].ring,
      [55, 55].ring,
      [65, 55, 55, 55].ring,
      [65, 66, 67, 55].ring,
    ]
    piss2 = [
      [0, 0].ring,
      [0, 0].ring,
      [0, 55].ring,
      [0, 0, 55, 55].ring
    ]
    pan *= -1
    pandiff *= -1
    amp = 1
    pi = 0
    use_synth :dtri
    if cnt < 15
      pis = piss1.choose
      print("b from piss1 " + pis.to_s)
    else
      pis = piss2.choose
      print("b from piss2 " + pis.to_s)
    end
    
    define :melody2 do
      play pis[pi], amp: amp, pan: pan, attack: 0.01, sustain: 0, release: 0.1
      pan += pandiff
      pi += 1
      amp *= 0.92
      sleep 0.125
    end
    
    32.times do
      melody2
    end
    sleep 3
  end
  cnt += 1
  sleep 2
end
