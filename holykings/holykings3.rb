
live_loop :holykings2 do
  in_thread do
    use_synth :mod_fm
    4.times do
      play 56, amp: 0.5, attack: 0.2, sustain: 0, release: 0.2
      sleep 0.25
    end
    use_synth :mod_fm
    2.times do
      play 58, amp: 0.4, attack: 0.4, sustain: 0.2, release: 0.6
      sleep 0.5
    end
  end
  in_thread do
    pan = -0.7
    amp = 1.5
    use_synth :mod_sine
    25.times do
      play 56, amp: amp, pan: pan, attack: 0.01, sustain: 0, release: 0.1
      sleep 0.125
      pan += 0.05
      amp *= 0.9
    end
  end
  sleep 3
end
