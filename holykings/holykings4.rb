pan = 0.1
pandiff = -0.02
pidiff = 1

live_loop :holykings4 do
  in_thread do
    pidiff *= -1
    use_synth :fm
    rel = 0.1
    pi = 60
    amp = 1
    8.times do
      play pi, amp: amp, attack: 0.001, sustain: 0, release: rel
      rel *= 1.5
      pi += pidiff
      amp *= 0.8
      sleep 0.25
    end
  end
  in_thread do
    pan *= -1
    pandiff *= -1
    amp = 0.3
    use_synth :hoover
    32.times do
      play 56, amp: amp, pan: pan, attack: 0.01, sustain: 0, release: 0.1
      sleep 0.125
      pan += pandiff
      amp *= 0.92
    end
  end
  sleep 3
end# Welcome to Sonic Pi v3.1

