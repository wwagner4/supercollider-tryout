live_loop :hilykings2 do
  use_synth :mod_fm
  4.times do
    play 56, amp: 0.2, attack: 0.1, sustain: 0, release: 0.2
    sleep 0.25
  end
  use_synth :mod_fm
  2.times do
    play 58, amp: 0.3, attack: 0.4, sustain: 0.2, release: 0.6
    sleep 0.5
  end
  pan = -0.7
  amp = 0.4
  use_synth :dsaw
  10.times do
    play 56, amp: amp, pan: pan, attack: 0.01, sustain: 0, release: 0.1
    sleep 0.125
    pan += 0.1
    amp *= 0.8
  end
end