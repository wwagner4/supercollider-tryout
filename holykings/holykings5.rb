
pan = 0.7
pandiff = -0.03
pidiff = 1

live_loop :holykings5 do
  pidiff *= -1
  pi += pidiff
  in_thread do
    pis = [65, 63, 61, 58, 58, 55, 55].ring
    use_synth :pluck
    rel = 0.1
    amp = 1
    pi = 0
    10.times do
      play pis[pi], amp: amp, attack: 0.01, sustain: 0, release: rel
      rel *= 1.7
      amp *= 0.8
      pi = pi + pidiff
      sleep 0.25
    end
  end
  in_thread do
    pis = [65, 63, 66, 66].ring
    pan *= -1
    pandiff *= -1
    amp = 1.5
    pi = 0
    use_synth :sine
    32.times do
      play pis[pi], amp: amp, pan: pan, attack: 0.01, sustain: 0, release: 0.1
      sleep 0.125
      pan += pandiff
      pi = pi + pidiff
      amp *= 0.92
    end
  end
  sleep 3
end
