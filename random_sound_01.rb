# Some random sounds

define :rdef do |seed|
  with_random_seed seed do
    [
      [0, rand, rand],
      [1, rand, rand],
      [2, rand, rand],
      [3, rand, rand],
      [4, rand, rand],
      [5, rand, rand],
      [6, rand, rand],
    ]
  end
end

define :s0 do |pitch, duration, sdef|
  use_synth :sine
  for sy in sdef
    in_thread do
      play pitch: pitch + (sy[0] * 11.9),
        duration: duration * sy[1],
        amp: sy[2]
    end
  end
end

define :sr do |pitch, duration, seed|
  d = rdef seed
  s0 pitch, duration, d
end

live_loop :t1 do
  k = 0
  100.times do
    pis = [20, 19, 18, 17, 16, 17, 18, 19 ].ring
    durs = [0.25, 0.5, 0.5, 1, 2].ring
    de = rdef k
    5.times do
      s0 pis[k], durs[k], de
      sleep durs[k]
      k = k + 1
    end
  end
end
