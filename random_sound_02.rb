# Some random sounds

define :rdef do |seed|
  with_random_seed seed do
    [
      [0, rand, rand, rand, rand],
      [1, rand, rand, rand, rand],
      [2.0, rand, rand, rand, rand],
      [3.0, rand, rand, rand, rand],
      [4.0, rand, rand, rand, rand],
      [5.5, rand, rand, rand, rand],
      [6.5, rand, rand, rand, rand],
      [7.5, rand, rand, rand, rand],
      [8.5, rand, rand, rand, rand],
      [9.6, rand, rand, rand, rand],
    ]
  end
end

define :s0 do |pitch, duration, sdef|
  use_synth :sine
  n = 1
  for sy in sdef
    in_thread do
      play pitch: pitch + (sy[0] * 12),
        duration: duration * sy[1],
        attack: duration * sy[2] * 0.5,
        sustain: duration * sy[3] * 0.2,
        amp: sy[4]
    end
    n = n+1
  end
end

define :sr do |pitch, duration, seed|
  d = rdef seed
  s0 pitch, duration, d
end

define :fdurs do
  [0.5, 0.5, 1, 0.25, 0.25, 0.5, 1].ring
end
define :fpis do
  [10, 12, 15, 15, 15, 12, 10, 10, 10].ring
end

live_loop :t1 do
  k = 0
  100.times do
    pis = fpis
    durs = fdurs
    de = rdef k
    7.times do
      s0 pis[k], durs[k], de
      sleep durs[k]
      k = k + 1
    end
  end
end
